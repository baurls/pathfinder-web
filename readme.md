# Description

This is a public copy of the ProjectInf Project "Pathfinder-Server" which yields the application "PathfinderWeb".

The zip files containes both the client and the server code.

# Authors
The original authors are  
- Shoma Kaiser | st142241@stud.uni-stuttgart.de
- Patrick Lindemann | st155665@stud.uni-stuttgart.de
- Enis Spahiu | st141173@stud.uni-stuttgart.de
